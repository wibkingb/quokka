#ifndef TEST_GRAV_RAD_PARTICLE_3D_HPP_ // NOLINT
#define TEST_GRAV_RAD_PARTICLE_3D_HPP_
/// \file test_grav_rad_particle_3D.hpp
/// \brief Defines a 3D test problem for radiating particles with gravity.
///

// external headers
#include <fmt/format.h>

// internal headers

// function definitions

#endif // TEST_GRAV_RAD_PARTICLE_3D_HPP_
