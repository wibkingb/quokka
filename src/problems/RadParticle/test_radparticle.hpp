#ifndef TEST_RADPARTICLE_HPP_ // NOLINT
#define TEST_RADPARTICLE_HPP_
/// \file test_radparticle.hpp
/// \brief Defines a 1D test problem for radiating particles.
///

// external headers
#ifdef HAVE_PYTHON
#include "util/matplotlibcpp.h"
#endif
#include <fmt/format.h>

// internal headers

#include "radiation/radiation_system.hpp"

// function definitions

#endif // TEST_RADPARTICLE_HPP_
