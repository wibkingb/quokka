#ifndef TEST_RADPARTICLE_2D_HPP_ // NOLINT
#define TEST_RADPARTICLE_2D_HPP_
/// \file test_radparticle_2D.hpp
/// \brief Defines a 2D test problem for radiating particles.
///

// external headers
#ifdef HAVE_PYTHON
#include "util/matplotlibcpp.h"
#endif
#include <fmt/format.h>

// internal headers

#include "radiation/radiation_system.hpp"

// function definitions

#endif // TEST_RADPARTICLE_2D_HPP_
