# Gallery

## QED III 


QED is a suite of 3D hydrodynamic simulations which follow supernova-driven outflows in varying galactic environments. The QED follows outflows from a $1$ kpc$^2$ patch of galaxy up to a vertical height of $\sim$ few kpc. In the third of a series of QED papers, [found here](https://ui.adsabs.harvard.edu/abs/2025arXiv250200929V/abstract), we explore how outflow properties of the outflows depend on three galactic environments: Solar neighbourhood, inner, and outer galaxy.


### Solar Neighbourhood

The initial gas density and temperature profiles are derived using Solar neighbourhood gas surface density, $13 M_{\odot}$ pc$^{-2}$. The SN rate is $6\times 10^{-5}$ yr$^{-1}$ and the gas cools with Solar (top) and sub-Solar (bottom) metallicity. Though box height ($8$ kpc) and the resolution ($2$ pc, uniform) are the same for both the metallicities, the nature of outflows in these two cases is quite different.

The Solar metallicity case:

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/1051348948?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; clipboard-write; encrypted-media" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="QED III - Solar Neighbourhood"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<br />

The sub-Solar metallicity case:

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/1051353575?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; clipboard-write; encrypted-media" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="QED III - Solar Neighbourhood, sub-Solar metallicity"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<br />

### Inner Galaxy

The initial gas surface density is $50 M_{\odot}$ pc$^{-2}$ and the SN rate is commensurately higher, $3\times 10^{-4}$ yr$^{-1}$. The domain size and resolution are identical to the Solar neighbourhood case, but note how the evolution for gas cooling at Solar (top) and sub-Solar (bottom) metallicities are closer in nature than the for the Solar neighbourhood case. 

The Solar metallicity case:

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/1051352748?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; clipboard-write; encrypted-media" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="QED III - Inner Galaxy"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<br />

The sub-Solar metallicity case:

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/1051356179?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; clipboard-write; encrypted-media" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="QED III - Inner Galaxy, sub-Solar Metallicity"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<br />

### Outer Galaxy

The intial gas surface density for these runs is $2.5 M_{\odot}$ pc$^{-2}$ for the Outer galaxy cases. Because the inital gas surface density is lower, the gas scale height of the warm phase is much larger. To accomodate the larger gas scale height the box size is increased to $16$ kpc in the vertical direction. The SNe go off in a relatively high density surrounding medium arresting development of large scale outflows. The mass loading factor is small.

The Solar-metallicity case looks like this:

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/1051354187?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; clipboard-write; encrypted-media" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="QED III - Outer Galaxy"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<br />

The sub-Solar metallicity outflows look like this:

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/1051358462?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; clipboard-write; encrypted-media" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="QED III - Outer Galaxy, sub-Solar Metallicity"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<br />

### Zoom-in

$1$ kpc zoom-in of the Solar neighbourhood case shows the spectacular rise and fall of clouds in the Solar neighbourhood! 

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/1051357669?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; clipboard-write; encrypted-media" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="QED III - zoomed in Solar Neighbourhood"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<br />

In the Inner galaxy, the zoom-in shows how the disc seems to be breathing in and out! 

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/1051358716?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; clipboard-write; encrypted-media" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="QED III - zoomed in Inner Galaxy"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
<br />
